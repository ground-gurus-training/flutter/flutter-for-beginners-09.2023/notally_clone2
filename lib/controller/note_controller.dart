import 'package:get/get.dart';
import 'package:notally_clone2/model/note_data.dart';

class NoteController extends GetxController {
  var notesData = [
    NoteData(title: 'Grocery List', note: 'Bread, Butter, Eggs'),
    NoteData(title: 'Movies', note: 'Avengers'),
    NoteData(
      title: 'Programming Languages',
      note: 'JavaScript, Dart, Java, C#',
    ),
  ].obs;

  add(NoteData data) {
    notesData.add(data);
  }
}
