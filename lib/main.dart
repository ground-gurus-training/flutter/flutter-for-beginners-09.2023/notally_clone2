import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:notally_clone2/controller/note_controller.dart';
import 'package:notally_clone2/pages/new_note.dart';
import 'package:notally_clone2/widgets/notes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  NoteController controller = Get.put(NoteController());
  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Notally'),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.search),
            ),
          ],
        ),
        drawer: const Drawer(
          child: Row(children: []),
        ),
        body: Obx(
          () => Column(
            children: [
              for (var noteData in controller.notesData) Notes(noteData)
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Get.to(NewNote());
          },
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
