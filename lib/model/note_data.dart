class NoteData {
  String title;
  String note;

  NoteData({this.title = '', this.note = ''});
}
