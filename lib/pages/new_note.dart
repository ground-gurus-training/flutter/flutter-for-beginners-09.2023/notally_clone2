import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:notally_clone2/controller/note_controller.dart';

import '../model/note_data.dart';

class NewNote extends StatelessWidget {
  NoteController controller = Get.put(NoteController());
  TextEditingController titleController = TextEditingController();
  TextEditingController noteController = TextEditingController();

  NewNote({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              controller.add(
                NoteData(
                  title: titleController.text,
                  note: noteController.text,
                ),
              );
              Get.back();
            },
            icon: const Icon(Icons.save),
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              decoration: const InputDecoration(
                border: InputBorder.none,
                hintText: 'Title',
              ),
              controller: titleController,
            ),
            const Divider(),
            TextField(
              decoration: const InputDecoration(
                border: InputBorder.none,
                hintText: 'Note',
              ),
              controller: noteController,
              maxLength: 512,
              maxLines: 10,
            ),
          ],
        ),
      ),
    );
  }
}
