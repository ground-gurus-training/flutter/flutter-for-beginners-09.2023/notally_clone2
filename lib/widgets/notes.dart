import 'package:flutter/material.dart';
import 'package:notally_clone2/model/note_data.dart';

class Notes extends StatelessWidget {
  NoteData data;
  Notes(this.data, {super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          ListTile(
            title: Text(data.title),
            subtitle: Text(data.note),
          )
        ],
      ),
    );
  }
}
